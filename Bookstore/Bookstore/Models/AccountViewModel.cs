﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bookstore.Models.AccountModel;

namespace Bookstore.Models
{
    public class AccountViewModel
    {
        public LoginModel LoginModel { get; set; }

        public UserProfile UserProfile { get; set; }
        
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("LoginModel.Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        

    }
}