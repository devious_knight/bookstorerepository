﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using Bookstore.Models.AccountModel;

namespace Bookstore.Models.DataAccess
{
    public class UserProfileDB
    {
        private string cs;
        public UserProfileDB() 
        {
            cs = WebConfigurationManager.ConnectionStrings["Bookstore"].ConnectionString;
        }
        public UserProfileDB(string cs)
        {
            this.cs = cs;
        }
   
    public IEnumerable<UserProfile> GetAllUserProfile()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("GetAllUserProfile", con);
            cmd.CommandType = CommandType.StoredProcedure;
            List<UserProfile> userProfiles = new List<UserProfile>();
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    UserProfile userProfile = new UserProfile
                    {
                        UserId = (int) reader["UserID"],
                        UserName = (string) reader["UserName"],
                        FullName = (string) reader["FullName"],
                        Address = (string) reader["Address"],
                        Email = (string) reader["Email"],
                        Phone = (string) reader["Phone"]
                    };

                    userProfiles.Add(userProfile);
                }
                reader.Close();
                return userProfiles;
            }
        }

        public void UpdateUserProfile(UserProfile userProfile)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("UpdateUserProfile", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("UserId", userProfile.UserId);
            cmd.Parameters.AddWithValue("UserName", userProfile.UserName);
            cmd.Parameters.AddWithValue("FullName", userProfile.FullName);
            cmd.Parameters.AddWithValue("Address", userProfile.Address);
            cmd.Parameters.AddWithValue("Phone", userProfile.Phone);
            cmd.Parameters.AddWithValue("Email", userProfile.Email);
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}