﻿using System.Collections.Generic;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Bookstore.Models.DataAccess
{
    public class AuthorBookDB
    {  private string cs;
        public AuthorBookDB() 
        {
            cs = WebConfigurationManager.ConnectionStrings["Bookstore"].ConnectionString;
        }
        public AuthorBookDB(string cs)
        {
            this.cs = cs;
        }
        public IEnumerable<AuthorBook> GetAllAuthorBook()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("GetAllAuthorBook", con) {CommandType = CommandType.StoredProcedure};
            List<AuthorBook> authorBooks = new List<AuthorBook>();
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    AuthorBook authorBook = new AuthorBook();
                    authorBook.Author_ID = (int)reader["Author_ID"];
                    authorBook.Book_ID = (int)reader["Book_ID"];

                    authorBooks.Add(authorBook);
                }
                reader.Close();
                return authorBooks;
            }
        }

        public void InsertAuthorBook(AuthorBook authorBook)
        {
            SqlConnection con =new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("InsertAuthorBook",con) {CommandType = CommandType.StoredProcedure};
            cmd.Parameters.AddWithValue("@Author_ID", authorBook.Author_ID);
            cmd.Parameters.AddWithValue("@Book_ID", authorBook.Book_ID);
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteAuthorBookByAuthor(int author_ID)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("DeleteAuthorBookByAuthor", con) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@Author_ID", author_ID);
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}