﻿using System.Collections.Generic;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Bookstore.Models.DataAccess
{
    public class GenreDB
    {
        private string cs;
        public GenreDB() 
        {
            cs = WebConfigurationManager.ConnectionStrings["Bookstore"].ConnectionString;
        }
        public GenreDB(string cs)
        {
            this.cs = cs;
        }
        public List<Genre> GetAllGenres()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("GetAllGenres", con);
            cmd.CommandType = CommandType.StoredProcedure;
            List<Genre> genres = new List<Genre>();
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Genre genre = new Genre();
                    genre.Genre_ID = (int)reader["Genre_ID"];
                    genre.Genre_Name = (string)reader["Genre_Name"];

                    genres.Add(genre);
                }
                reader.Close();
                return genres;
            }
        }

        public void InsertGenre(Genre genre)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("InsertGenre", con) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@Genre_Name", genre.Genre_Name);
            
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }

        }

        public void UpdateGenre(Genre genre)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("UpdateGenre", con) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@Genre_ID", genre.Genre_ID);
            cmd.Parameters.AddWithValue("@Genre_Name", genre.Genre_Name);
           
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteGenre(int genre_ID)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("DeleteUpdGenre", con) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@Genre_ID", genre_ID);
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}