﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Bookstore.Models.DataAccess
{
    public class AuthorDB
    {
        private string cs;
        public AuthorDB() 
        {
            cs = WebConfigurationManager.ConnectionStrings["Bookstore"].ConnectionString;
        }
        public AuthorDB(string cs)
        {
            this.cs = cs;
        }

        public IEnumerable<Author> GetAuthorByBook(int book_ID)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("GetAuthorByBook", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Book_ID", book_ID);
            List<Author> authors = new List<Author>();
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Author author = new Author();
                    author.Author_ID = (int)reader["Author_ID"];
                    author.First_Name = (string)reader["First_Name"];
                    if (reader["Second_Name"] == DBNull.Value)
                    {
                        author.Second_Name = string.Empty;
                    }
                    else
                    {
                        author.Second_Name = (string)reader["Second_Name"];
                    }
                    author.Last_Name = (string)reader["Last_Name"];
                    authors.Add(author);
                }
                reader.Close();
                return authors;
            }
        }

        public IEnumerable<Author> GetAllAuthors()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("GetAllAuthors", con);
            cmd.CommandType = CommandType.StoredProcedure;
            List<Author> authors = new List<Author>();
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Author author=new Author();
                    author.Author_ID = (int)reader["Author_ID"];
                    author.First_Name = (string)reader["First_Name"];
                        if (reader["Second_Name"] == DBNull.Value)
                        { 
                            author.Second_Name = string.Empty; 
                        }
                        else
                        { 
                            author.Second_Name = (string)reader["Second_Name"]; 
                        }
                    author.Last_Name = (string)reader["Last_Name"];
                    authors.Add(author);
                }
                reader.Close();
                return authors;
            }
        }
        public void InsertAuthor(Author author)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("InsertAuthor", con) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@First_Name", author.First_Name);
            cmd.Parameters.AddWithValue("@Second_Name", author.Second_Name ?? " ");
            cmd.Parameters.AddWithValue("@Last_Name", author.Last_Name);
            
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }

        }

        public void UpdateAuthor(Author author)
        {
            SqlConnection con=new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("UpdateAuthor", con) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@Author_ID", author.Author_ID);
            cmd.Parameters.AddWithValue("@First_Name", author.First_Name);
            cmd.Parameters.AddWithValue("@Second_Name", author.Second_Name ?? " ");
            cmd.Parameters.AddWithValue("@Last_Name", author.Last_Name);
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteAuthor(int author_ID)
        {
            SqlConnection con =new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("DeleteAuthor",con){CommandType = CommandType.StoredProcedure};
            cmd.Parameters.AddWithValue("@Author_ID", author_ID);
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}