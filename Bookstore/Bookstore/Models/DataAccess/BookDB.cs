﻿using System.Collections.Generic;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.Ajax.Utilities;

namespace Bookstore.Models.DataAccess
{
    public class BookDB
    {
        private string cs;
        public BookDB() 
        {
            cs = WebConfigurationManager.ConnectionStrings["Bookstore"].ConnectionString;
        }
        public BookDB(string cs)
        {
            this.cs = cs;
        }
        public IEnumerable<Book> GetAllBooks()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("GetAllBooks", con);
            cmd.CommandType = CommandType.StoredProcedure;
            List<Book> books = new List<Book>();
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Book book = new Book();
                    book.Book_ID = (int)reader["Book_ID"];
                    book.Name = (string)reader["Name"];
                    book.Price = (decimal)reader["Price"];
                    book.Number_Of_Pages = (decimal)reader["Number_Of_Pages"];
                    book.Book_Weight = (decimal)reader["Book_Weight"];
                    if (reader["About"] == DBNull.Value)
                    {
                        book.About = string.Empty;
                    }
                    else
                    {
                        book.About = (string)reader["About"];
                    }
                    book.Genre_ID = (int)reader["Genre_ID"];
                    book.ImageData = reader["ImageData"] == DBNull.Value ? null : (byte[]) reader["ImageData"];
                    book.ImageMimeType = reader["ImageMimeType"] == DBNull.Value ? string.Empty : (string)reader["ImageMimeType"];
                    //
                //    if (reader["ImageData"] == DBNull.Value)
                    //book.ImageData = (byte[])reader["ImageData"];
                  //  book.ImageMimeType = (string)reader["ImageMimeType"];
                    books.Add(book);
                }
                reader.Close();
                return books;
            }
           
        }

        public IEnumerable<Book> GetBookByAuthor(int author_ID)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("GetBookByAuthor", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Author_ID", author_ID);
            List<Book> books = new List<Book>();
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Book book = new Book();
                    book.Book_ID = (int)reader["Book_ID"];
                    book.Name = (string)reader["Name"];
                    book.Price = (decimal)reader["Price"];
                    book.Number_Of_Pages = (decimal)reader["Number_Of_Pages"];
                    book.Book_Weight = (decimal)reader["Book_Weight"];
                    book.About = (string)reader["About"];
                    book.Genre_ID = (int)reader["Genre_ID"];
                    book.ImageData = reader["ImageData"] == DBNull.Value ? null : (byte[])reader["ImageData"];
                    book.ImageMimeType = reader["ImageMimeType"] == DBNull.Value ? string.Empty : (string)reader["ImageMimeType"];
                    books.Add(book);
                }
                reader.Close();
                return books;
            }
        }

        public void InsertBook(Book book){
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("InsertBook", con) {CommandType = CommandType.StoredProcedure};
            cmd.Parameters.AddWithValue("@Name", book.Name);
            cmd.Parameters.AddWithValue("@Price", book.Price);
            cmd.Parameters.AddWithValue("@Number_Of_Pages", book.Number_Of_Pages);
            cmd.Parameters.AddWithValue("@Book_Weight", book.Book_Weight);
            cmd.Parameters.AddWithValue("@About", book.About ?? "Без описания");
            cmd.Parameters.AddWithValue("@Genre_ID", book.Genre_ID);
            if (book.ImageMimeType.IsNullOrWhiteSpace())
            {
                cmd.Parameters.AddWithValue("@ImageData", DBNull.Value);
                cmd.Parameters.AddWithValue("@ImageMimeType", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ImageData", book.ImageData);
                cmd.Parameters.AddWithValue("@ImageMimeType", book.ImageMimeType);
            }

            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }

        }
        public void UpdateBook(Book book)
        {
            
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("UpdateBook", con) {CommandType = CommandType.StoredProcedure};
            cmd.Parameters.AddWithValue("@Book_ID", book.Book_ID);
            cmd.Parameters.AddWithValue("@Name", book.Name);
            cmd.Parameters.AddWithValue("@Price", book.Price);
            cmd.Parameters.AddWithValue("@Number_Of_Pages", book.Number_Of_Pages);
            cmd.Parameters.AddWithValue("@Book_Weight", book.Book_Weight);
            cmd.Parameters.AddWithValue("@About", book.About ?? "Без описания");
            cmd.Parameters.AddWithValue("@Genre_ID", book.Genre_ID);
            if (book.ImageMimeType.IsNullOrWhiteSpace())
            {
                cmd.Parameters.Add("@ImageData", SqlDbType.VarBinary, -1);
                cmd.Parameters["@ImageData"].Value = DBNull.Value;
                cmd.Parameters.AddWithValue("@ImageMimeType", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ImageData", book.ImageData);
                cmd.Parameters.AddWithValue("@ImageMimeType", book.ImageMimeType);
            }
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteBook(int book_ID)
        {
            SqlConnection con =new SqlConnection(cs);
            SqlCommand cmd=new SqlCommand("DeleteBook",con) {CommandType = CommandType.StoredProcedure};
            cmd.Parameters.AddWithValue("Book_ID", book_ID);
            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}