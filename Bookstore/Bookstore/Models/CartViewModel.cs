﻿using Bookstore.Models.CartModel;

namespace Bookstore.Models
{
    public class CartViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}