﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Bookstore.Interfaces;
using Bookstore.Models.AccountModel;
using Bookstore.Models.DataAccess;

namespace Bookstore.Models.Context
{
    public class BookstoreRepository:IBookstoreRepository
    {
        //BookstoreContext context=new BookstoreContext();
        BookDB books=new BookDB();
        GenreDB genres= new GenreDB();
        AuthorDB authors=new AuthorDB();
        AuthorBookDB authorBooks = new AuthorBookDB();
        UserProfileDB userProfile=new UserProfileDB();
        public IQueryable<UserProfile> UserProfiles {
            get { return userProfile.GetAllUserProfile().AsQueryable(); }
        }

        public IQueryable<Book> Books
        {
            get { return books.GetAllBooks().AsQueryable(); }
        }
        
        public IQueryable<Genre> Genres
        {
            get { return genres.GetAllGenres().AsQueryable(); }
        }

        public IQueryable<Author> Authors 
        {
            get { return authors.GetAllAuthors().AsQueryable(); }
        }
        public IQueryable<AuthorBook> GetAllAuthorBook
        {
            get { return authorBooks.GetAllAuthorBook().AsQueryable(); }
        }

        public IQueryable<Book> GetBookByAuthor(int author_ID)
        {
            return books.GetBookByAuthor(author_ID).AsQueryable();
        }

        public IQueryable<Author> GetAuthorByBook(int book_ID)
        {
            return authors.GetAuthorByBook(book_ID).AsQueryable();
        }

        public void SaveChanges(Book book)
        {
            if (book.Book_ID == 0)
            {
                books.InsertBook(book);
            }
            else
            {
                books.UpdateBook(book);
            }
        }

        public void Delete(Book book)
        {
            books.DeleteBook(book.Book_ID);
        }


        public void SaveChanges(Author author)
        {
            if (author.Author_ID == 0)
            {
                authors.InsertAuthor(author);
            }
            else
            {
                authors.UpdateAuthor(author);
            }
        }

        public void Delete(Author author)
        {
            authors.DeleteAuthor(author.Author_ID);
        }


        public void SaveChanges(Genre genre)
        {
            if (genre.Genre_ID == 0)
            {
                genres.InsertGenre(genre);
            }
            else
            {
                genres.UpdateGenre(genre);
            }
        }

        public void Delete(Genre genre)
        {
           genres.DeleteGenre(genre.Genre_ID);
        }

        public void InsertAuthorBook(AuthorBook authorBook)
        {
            authorBooks.InsertAuthorBook(authorBook);
        }

        public void DeleteAuthorBookByAuthor(int author_ID)
        {
            authorBooks.DeleteAuthorBookByAuthor(author_ID);
        }


        public void SaveChanges(UserProfile userProfile)
        {
            this.userProfile.UpdateUserProfile(userProfile);
        }
    }
}