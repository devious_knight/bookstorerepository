﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using Bookstore.Interfaces;
using Bookstore.Models.AccountModel;
using Bookstore.Models.CartModel;
using Bookstore.Models.Orders;

namespace Bookstore.Models.Context
{
    public class EmailOrderProcessor:IOrderProcessor
    {

        public void ProcessOrder(Cart cart, ShippingDetails shippingDetails)
        {
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.EnableSsl = true;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 587;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential("myfakebookstore", "myfakepassword");


                StringBuilder message = new StringBuilder().AppendLine("Новый заказ");
                foreach (var myCart in cart.MyCarts)
                {
                    message.AppendFormat("{0} x {1}, сумма: {2} ", myCart.Quantity, myCart.Book.Name,
                        myCart.Book.Price * myCart.Quantity).AppendLine("");
                }

                message.AppendLine("").AppendFormat("Общая сумма заказа: {0}", cart.TotalValue()).AppendLine("").AppendLine("")
                    .AppendLine("Доставка")
                    .AppendLine(shippingDetails.Name)
                    .AppendLine(shippingDetails.Address)
                    .AppendLine(shippingDetails.Phone)
                    .AppendLine(shippingDetails.Email);
                MailMessage mailMessage = new MailMessage("order@MyBookstore.com", "myfakebookstore@gmail.com", "Новый заказ",
                    message.ToString());
                smtpClient.Send(mailMessage);
            }
        }
    }
}