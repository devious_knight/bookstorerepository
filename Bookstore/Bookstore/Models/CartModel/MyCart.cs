﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookstore.Models.CartModel
{
    public class MyCart
    {
        public Book Book { get; set; }
        public int  Quantity { get; set; }
    }
}