﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookstore.Models.CartModel
{
    public class Cart
    {
        private List<MyCart> myCarts=new List<MyCart>();

        public void AddItem(Book book, int quantity)
        {
            MyCart myCart = myCarts.Where(x => x.Book.Book_ID == book.Book_ID).FirstOrDefault();
            if (myCart == null)
            {
                myCarts.Add(new MyCart{Book = book,Quantity = quantity});
            }
            else
            {
                myCart.Quantity += quantity;
            }
        }

        public void Remove(Book book)
        {
            myCarts.RemoveAll(x => x.Book.Book_ID == book.Book_ID);
        }

        public decimal TotalValue()
        {
            return myCarts.Sum(x => x.Book.Price*x.Quantity);
        }

        public void Clear()
        {
            myCarts.Clear();
        }

        public IEnumerable<MyCart> MyCarts {
            get { return myCarts; }
        }
    }
}