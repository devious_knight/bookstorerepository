﻿
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Bookstore.Models
{
    public class Book
    {
        [HiddenInput(DisplayValue = false)]
        public int Book_ID { get; set; }
        [Required]
        [Display(Name = "Название книги")]
        public string Name { get; set; }
         [Required]
        
        public decimal Price { get; set; }
         [Required]
        public decimal Number_Of_Pages { get; set; }
         [Required]
        public decimal Book_Weight { get; set; }
        [DataType(DataType.MultilineText)]
        public string About { get; set; }
         [Required]
        public int Genre_ID { get; set; }

        public byte[] ImageData { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string ImageMimeType { get; set; }
    }
}