﻿

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Bookstore.Models
{
    public class Author
    {
        [HiddenInput(DisplayValue = false)]
        public int Author_ID { get; set; }
        [Required]
        public string First_Name { get; set; }
        
        public string Second_Name { get; set; }
        [Required]
        [Display(Name = "Фамилия автора")]
        public string Last_Name { get; set; }
    }
}