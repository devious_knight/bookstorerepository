﻿
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Bookstore.Models
{
    public class Genre
    {
        [HiddenInput(DisplayValue = false)]
        public int Genre_ID { get; set; }
        [Required]
        [Display(Name = "Жанр")]
        public string Genre_Name { get; set; }
    }
}