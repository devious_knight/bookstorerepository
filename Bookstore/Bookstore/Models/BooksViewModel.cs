﻿using System.Collections.Generic;

namespace Bookstore.Models
{
    public class BooksViewModel
    {
        public Book Book { get; set; }
        public Genre Genre { get; set; }
        public IEnumerable<Book> Books { get; set; }
        public PagingInfo PagingInfo{ get; set; }
        public IEnumerable<Author> Authors { get; set; }
        public IEnumerable<Genre> Genres { get; set; }
        public int CurrentGenre { get; set; }
    }
}