﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bookstore.Interfaces;
using Bookstore.Models;
using Bookstore.Models.AccountModel;
using Bookstore.Models.CartModel;
using Bookstore.Models.Orders;
using WebMatrix.WebData;

namespace Bookstore.Controllers
{
    public class CartController : Controller
    {
        //
        // GET: /Cart/
        private IBookstoreRepository _repository;
        private IOrderProcessor _orderProcessor;
        public CartController(IBookstoreRepository repository,IOrderProcessor order)
        {
            _repository = repository;
            _orderProcessor = order;

        }

        public RedirectToRouteResult AddToCart(Cart cart,int book_Id,string returnUrl)
        {
            Book book = _repository.Books.First(x => x.Book_ID == book_Id);
            if (book != null)
            {
                cart.AddItem(book,1);
            }
            return RedirectToAction("Index",new {returnUrl});
        }

        public RedirectToRouteResult RemoveFromCart(Cart cart,int book_Id, string returnUrl)
        {
            Book book = _repository.Books.First(x => x.Book_ID == book_Id);
            if (book != null)
            {
                cart.Remove(book);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

       

        public ActionResult Index(Cart cart,string returnUrl)
        {
            return View(new CartViewModel {Cart = cart, ReturnUrl = returnUrl});
        }

        public PartialViewResult InMyCart(Cart cart)
        {
            return PartialView(cart);
        }

        public ViewResult Order()
        {
            if (WebSecurity.HasUserId)
            {
                UserProfile userProfile = _repository.UserProfiles.First(x => x.UserId == WebSecurity.CurrentUserId);
                ShippingDetails shippingDetails=new ShippingDetails()
                {Address = userProfile.Address,
                    Email = userProfile.Email,
                    Name =userProfile.FullName,
                    Phone = userProfile.Phone
                };
                return View(shippingDetails);
            }
            else
            {
                return View(new ShippingDetails());
            }
        }

        [HttpPost]
        public ViewResult Order(Cart cart,ShippingDetails shipingDetails)
        {
            if (cart.MyCarts.Count() == 0)
            {
                ModelState.AddModelError("","Корзина пуста");
            }
            if (ModelState.IsValid)
            {
                _orderProcessor.ProcessOrder(cart, shipingDetails);
                cart.Clear();
                return View("OrderCompleted");
            }
            else
            {
                return View(shipingDetails);
            }
        }
    }
}
