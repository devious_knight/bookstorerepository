﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Bookstore.Interfaces;
using Bookstore.Models;
using Bookstore.Models.AccountModel;
using WebMatrix.WebData;

namespace Bookstore.Controllers
{
    public class AccountController : Controller
    {
        private IBookstoreRepository _repository;
        public AccountController(IBookstoreRepository repo)
        {
            _repository =repo;
        }
        
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl="/")
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                
                return Redirect(returnUrl);
            }

           ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }
       
        [Authorize]
        public ActionResult AccountInfo()
        {
            return View(_repository.UserProfiles.First(x => x.UserId == WebSecurity.CurrentUserId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff(string returnUrl)
        {
            WebSecurity.Logout();

            return Redirect(returnUrl);
        }

        [AllowAnonymous]
        public ActionResult Register(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(string returnUrl,Registration registration)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    WebSecurity.CreateUserAndAccount(registration.UserName, registration.Password);
                    WebSecurity.Login(registration.UserName, registration.Password);
                    Roles.AddUserToRole(registration.UserName, "User");
                    _repository.SaveChanges(new UserProfile()
                    {
                        Address = registration.Address,
                        Email = registration.Email,
                        FullName = registration.FullName,
                        Phone = registration.Phone,
                        UserId = WebSecurity.GetUserId(registration.UserName),
                        UserName = registration.UserName
                    });
                    return Redirect(returnUrl);
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
                
            }

            return View(registration);
            
            

        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Пользователь с таким ником уже существует.Введите другой ник.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
    }
}
