﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bookstore.Interfaces;
using Bookstore.Models;

namespace Bookstore.Controllers
{
    public class AuthorController : Controller
    {
        private const int PageSize = 5;
        //
        // GET: /Author/
        private IBookstoreRepository _repository;
        public AuthorController(IBookstoreRepository repo)
        {
            _repository = repo;
        }
        public ActionResult GetAuthors(int page=1)
        {
            BooksViewModel repo=new BooksViewModel()
            {
                Authors = _repository.Authors.OrderBy(p => p.Last_Name)
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize),
                        PagingInfo = new PagingInfo()
                        {
                            CurrentPage = page,
                            ItemsPerPage = PageSize,
                            TotalItems = _repository.Authors.Count()
                        }
            };
            return View(repo);
        }

        public ActionResult AuthorsBooks(int author_ID=0)
        {
            if (author_ID != 0)
            {
                return View(_repository.GetBookByAuthor(author_ID).ToList());
            }
            return RedirectToAction("GetAuthors");
        }

    }
}
