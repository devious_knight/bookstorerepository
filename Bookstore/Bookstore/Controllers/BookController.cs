﻿using System;
using System.Linq;
using System.Web.Mvc;
using Bookstore.Interfaces;
using Bookstore.Models;

namespace Bookstore.Controllers
{
    public class BookController : Controller
    {
        private const int PageSize = 5;

        private IBookstoreRepository _repository;
        public BookController(IBookstoreRepository repository )
        {
            _repository = repository;
        }
        
        public FileContentResult GetImage(int book_ID)
        {
            Book book = _repository.Books.First(x => x.Book_ID == book_ID);
            if (book != null)
            {
                return File(book.ImageData, book.ImageMimeType);
            }
            else
            {
                return null;
            }
        }

        public ActionResult BookInfo (int book_Id=0)
        {
            if (book_Id!=0)
            {
                return View(new BooksViewModel()
                {
                    Book=_repository.Books.First(x=>x.Book_ID==book_Id),
                    Authors = _repository.GetAuthorByBook( book_Id),
                    Genre = _repository.Genres.First(x => x.Genre_ID ==_repository.Books.First(b => b.Book_ID == book_Id).Genre_ID)
                });
            }
            return RedirectToAction("GetBooks");
        }

        public ActionResult GetBooks(string searchString, int genre_Id = 0, int page = 1)
        {
            ViewBag.SearchString = searchString;
            if (!String.IsNullOrEmpty(searchString))
            {
                BooksViewModel viewModel = new BooksViewModel
                {
                    Books = _repository.Books.Where(s=>s.Name.ToUpper().Contains(searchString.ToUpper()))
                        .Where(p => genre_Id == 0 || p.Genre_ID == genre_Id)
                        .OrderBy(p => p.Name)
                        .Skip((page - 1)*PageSize)
                        .Take(PageSize),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems =genre_Id == 0
                                ? _repository.Books.Count()
                                : _repository.Books.Where(b => b.Genre_ID == genre_Id).Count()
                        
                    },
                    CurrentGenre = genre_Id

                };
                return View(viewModel);
            }
            else
            {
                BooksViewModel viewModel = new BooksViewModel
                {
                    Books = _repository.Books
                        .Where(p => genre_Id == 0 || p.Genre_ID == genre_Id)
                        .OrderBy(p => p.Name)
                        .Skip((page - 1)*PageSize)
                        .Take(PageSize),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems =
                            genre_Id == 0
                                ? _repository.Books.Count()
                                : _repository.Books.Where(b => b.Genre_ID == genre_Id).Count()
                        
                    },
                    CurrentGenre = genre_Id

                }; 
                return View(viewModel);
            }
            
        }

    }
}
