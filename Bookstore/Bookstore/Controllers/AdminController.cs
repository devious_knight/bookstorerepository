﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bookstore.Interfaces;
using Bookstore.Models;

namespace Bookstore.Controllers
{
    [Authorize(Roles="Administrator")]
    public class AdminController : Controller
    {
        private const int PageSize = 5;

        
        private IBookstoreRepository _repository;

        public AdminController(IBookstoreRepository repository)
        {
            
            _repository = repository;
        }

        
        public ActionResult InsertAuthorBookByBook(int book_ID,int author_ID,string returnUrl)
        {
            bool flag = Enumerable.Any(_repository.GetAllAuthorBook.Where(x => x.Book_ID == book_ID), a => a.Author_ID == author_ID);
            if (!flag)
            {
                _repository.InsertAuthorBook(new AuthorBook() {Author_ID = author_ID, Book_ID = book_ID});
                return Redirect(returnUrl);
            }
            else
            {
                return Redirect(returnUrl);
            }
        }  

        public ActionResult DeleteAuthorBookByAuthor(int author_ID,string returnUrl)
        {
            _repository.DeleteAuthorBookByAuthor(author_ID);
            return Redirect(returnUrl);
        }

        #region Books

        public ActionResult Index(int page=1)
        {
            ViewBag.AdminBooksPage = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = PageSize,
                TotalItems = _repository.Books.Count()
            };

            return View(_repository.Books.OrderBy(x => x.Name).Skip((page - 1) * PageSize).Take(PageSize));
        }
        
        public ActionResult Edit(int book_ID)
        {
            ViewData["Genres"] = new SelectList(_repository.Genres, "Genre_ID", "Genre_Name",
                _repository.Books.First(x => x.Book_ID == book_ID).Genre_ID);
             ViewData["Authors"] = _repository.GetAuthorByBook(book_ID);
            BooksViewModel model = new BooksViewModel() { Book = _repository.Books.First(x => x.Book_ID == book_ID),
                Authors=_repository.Authors};
            return View(model); // return View(_repository.Books.First(x => x.Book_ID == book_ID));
        }

        [HttpPost]
        public ActionResult Edit(BooksViewModel model, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    model.Book.ImageMimeType = image.ContentType;
                    model.Book.ImageData = new byte[image.ContentLength];
                    image.InputStream.Read(model.Book.ImageData, 0, image.ContentLength);
                }
                else
                {
                    model.Book.ImageData = _repository.Books.First(x => x.Book_ID == model.Book.Book_ID).ImageData;
                    model.Book.ImageMimeType = _repository.Books.First(x => x.Book_ID == model.Book.Book_ID).ImageMimeType;
                }


                _repository.SaveChanges(model.Book);
                return RedirectToAction("Index");
            }
            
            return View();
        }

   

        public ActionResult Create()
        {
            ViewData["Genres"] = new SelectList(_repository.Genres, "Genre_ID", "Genre_Name");
            return View("Create", new Book());
        }

        [HttpPost]
        public ActionResult Create(Book book)
        {
            if (ModelState.IsValid)
            {
                _repository.SaveChanges(book);
                return RedirectToAction("Index");
            }
            else
            {

                return View(book);
            }

        }

        public ActionResult Delete(int book_ID)
        {
            _repository.Delete(_repository.Books.First(x => x.Book_ID == book_ID));
            return RedirectToAction("Index");
        }

        #endregion

        #region Authors
        public ActionResult Authors(int page=1)
        {
            ViewBag.AdminAuthorsPage = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = PageSize,
                TotalItems = _repository.Books.Count()
            };
            return View(_repository.Authors.OrderBy(x=>x.Last_Name).Skip((page-1)*PageSize).Take(PageSize));
        }

        public ActionResult EditAuthor(int author_ID)
        {

            return View(_repository.Authors.First(x => x.Author_ID == author_ID));
        }

        [HttpPost]
        public ActionResult EditAuthor(Author author)
        {
            if (ModelState.IsValid)
            {
                _repository.SaveChanges(author);
                return RedirectToAction("Authors");
            }
            else
            {

                return View(author);
            }
        }

        public ActionResult CreateAuthor()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateAuthor(Author author)
        {
            if (ModelState.IsValid)
            {
                _repository.SaveChanges(author);
                return RedirectToAction("Authors");
            }
            else
            {

                return View(author);
            }
          
        }

        public ActionResult DeleteAuthor(int author_ID)
        {
            _repository.Delete(_repository.Authors.First(x => x.Author_ID == author_ID));
            return RedirectToAction("Authors");
        }

        #endregion 

        #region Genres

        public ActionResult Genres()
        {
            return View(_repository.Genres);
        }

        public ActionResult EditGenre(int genre_ID)
        {
            return View(_repository.Genres.First(x => x.Genre_ID == genre_ID));
        }

        [HttpPost]
        public ActionResult EditGenre(Genre genre)
        {
            if (ModelState.IsValid)
            {
                _repository.SaveChanges(genre);
                return RedirectToAction("Genres");
            }
            else
            {
                return View(genre);
            }

        }

        public ActionResult DeleteGenre(int genre_ID)
        {
            if (genre_ID != 9) //чтобы в таблице был хотя бы один жанр
            {
                _repository.Delete(_repository.Genres.First(x => x.Genre_ID == genre_ID));
                return RedirectToAction("Genres");
            }
            else
            {
                return RedirectToAction("Genres");
            }
        }

        public ActionResult CreateGenre()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateGenre(Genre genre)
        {
            if (ModelState.IsValid)
            {
                _repository.SaveChanges(genre);
                return RedirectToAction("Genres");
            }
            else
            {

                return View(genre);
            }

        }

        #endregion

    }
}
