﻿using System.Web.Mvc;
using Bookstore.Interfaces;

namespace Bookstore.Controllers
{
    public class NavigationController : Controller
    {
       

        private IBookstoreRepository _repository;

        public NavigationController(IBookstoreRepository repository)
        {
            _repository = repository;
        }
        public PartialViewResult Menu()
        {
            //IEnumerable<string> genres = _repository.Genres
            //    .Select(x => x.Genre_Name)
            //    .Distinct()
            //    .OrderBy(x=>x);
            return PartialView(_repository.Genres);
        }

    }
}
