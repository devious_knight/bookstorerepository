﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bookstore.Models;
using Bookstore.Models.AccountModel;

namespace Bookstore.Interfaces
{
    public interface IBookstoreRepository   
    {
        IQueryable<Book> Books { get; }
        IQueryable<Genre> Genres { get; }
        IQueryable<Author> Authors { get; }
        IQueryable<AuthorBook> GetAllAuthorBook { get; }
        IQueryable<UserProfile> UserProfiles { get; }

        IQueryable<Book> GetBookByAuthor(int author_ID);
        IQueryable<Author> GetAuthorByBook(int book_ID);

        void SaveChanges(Book book);
        void Delete(Book book);
        void SaveChanges(Author author);
        void Delete(Author author);
        void SaveChanges(Genre genre);
        void Delete(Genre genre);
        void InsertAuthorBook(AuthorBook authorBook);
        void DeleteAuthorBookByAuthor(int author_ID);
        void SaveChanges(UserProfile userProfile);

    }
}
