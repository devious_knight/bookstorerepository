﻿using Bookstore.Models.CartModel;
using Bookstore.Models.Orders;

namespace Bookstore.Interfaces
{
   public interface IOrderProcessor
   {
       void ProcessOrder(Cart cart, ShippingDetails shippingDetails);
   }
}
